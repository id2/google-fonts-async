<?php

/**
  Plugin Name: Google Fonts Async
  Plugin URI: @LATER
  Description:
  Version: 0.0.7
  Author: Guillaume Focheux idsquare
  Author URI: http://guillaume-focheux.fr
  Licence: GPLv2
 */
defined( 'ABSPATH' ) or die( 'Doh?' ) ;

/**
 *  Plugin main constant
 */
define( 'GF_ASYNC_SLUG', 'google-fonts-async' ) ;

/**
 *  special slug with underscore for trigger
 */
define( 'GF_ASYNC_SLUG_', str_replace( '-', '_', GF_ASYNC_SLUG ) ) ;
/**
 * settings option name in database
 */
define( 'GF_ASYNC_SETTINGS', GF_ASYNC_SLUG_ . '_settings' ) ;

// Plugin process constant

/**
 * Plugin main file
 */
define( 'GF_ASYNC_FILE', __FILE__ ) ;

/**
 * Plugin main file's path
 */
define( 'GF_ASYNC_PATH', realpath( plugin_dir_path( GF_ASYNC_FILE ) ) . '/' ) ;

/**
 * Plugin functions path
 */
define( 'GF_ASYNC_FUNCTIONS_PATH', realpath( GF_ASYNC_PATH . 'functions' ) . '/' ) ;

/**
 * Plugin classes path
 */
define( 'GF_ASYNC_CLASSES_PATH', realpath( GF_ASYNC_PATH . 'classes' ) . '/' ) ;

/**
 * Plugin views path
 */
define( 'GF_ASYNC_VIEWS_PATH', realpath( GF_ASYNC_PATH . 'views' ) . '/' ) ;

/**
 * Plugin admin path
 */
define( 'GF_ASYNC_ADMIN_PATH', realpath( GF_ASYNC_PATH . 'admin' ) . '/' ) ;

/**
 * Plugin assets path
 */
define( 'GF_ASYNC_ASSETS_PATH', realpath( GF_ASYNC_PATH . 'assets' ) . '/' ) ;

// Plugin web constant

/**
 * Plugin main file URL
 */
define( 'GF_ASYNC_URL', plugin_dir_url( GF_ASYNC_FILE ) ) ;

/**
 * Plugin assets URL
 */
define( 'GF_ASYNC_ASSETS_URL', GF_ASYNC_URL . 'assets/' ) ;


// Plugin Load
add_action( 'plugins_loaded', '_' . GF_ASYNC_SLUG_ . '_init' ) ;

/**
 * Plugin initialisation
 *
 * @return void
 */
function _google_fonts_async_init()
{
    // Autosave isn't part of plugin
    if ( defined( 'DOING_AUTOSAVE' ) ) {
        return ;
    }

    if ( ! get_site_option( GF_ASYNC_SETTINGS ) ) {
        _google_fonts_async_install() ;
    }

    // // @TODO Translate
    // load_plugin_textdomain(GF_ASYNC_SLUG, false, GF_ASYNC_FILE . '/languages' );
    // Autoload
    // if version_compare(PHP_VERSION, '5.3', '<')
    require_once '_autoload52.php' ;
    // else version with namespace
    // require_once '_autoload.php' ;
    //Require front stuff
    require_once GF_ASYNC_FUNCTIONS_PATH . 'functions.php' ;

    //Require back stuff
    if ( is_admin() ) {
        require_once GF_ASYNC_FUNCTIONS_PATH . 'functions-admin.php' ;
    }
    else {
        require_once GF_ASYNC_FUNCTIONS_PATH . 'functions-user.php' ;
    }

    /**
     * Trigger for loaded events
     *
     */
    do_action( GF_ASYNC_SLUG_ . '_loaded' ) ;
}

register_deactivation_hook( __FILE__, '_' . GF_ASYNC_SLUG_ . '_desactivate' ) ;
register_uninstall_hook( __FILE__, '_' . GF_ASYNC_SLUG_ . '_uninstall' ) ;

/**
 * Plugin install function
 *
 * @return void
 */
function _google_fonts_async_install()
{
    // Create Options
    add_site_option( GF_ASYNC_SETTINGS, array(
        'fonts-selected' => array(
            'google' => array(),
            'typekit' => array()
        ),
        'google' => array( 'apikey' => '' ),
        'typekit' => array( 'apikey' => '' )
    ) ) ;
}

/**
 * Plugin desactivate function
 *
 * @return void
 */
function _google_fonts_async_desactivate()
{
    // Clean only transient because they are weight

    delete_transient( GF_ASYNC_SLUG_ . '_googlefonts_font_result' ) ;
    delete_transient( GF_ASYNC_SLUG_ . '_typekit_font_result' ) ;
}

/**
 * Plugin uninstall function
 *
 * @return void
 */
function _google_fonts_async_uninstall()
{
    // Clean Options
    delete_site_option( GF_ASYNC_SETTINGS ) ;

    // Clean transient
    delete_transient( GF_ASYNC_SLUG_ . '_googlefonts_font_result' ) ;
    delete_transient( GF_ASYNC_SLUG_ . '_typekit_font_result' ) ;
}
