<?php
/**
 * View for plugin config
 */
$font_category = array() ;
?>
<div class="gfa-wrapper">
    <header>
        <h1>Google Fonts Async</h1>
        <blockquote>La font c'est mon dada! <span>Arnold S.</span></blockquote>
    </header>
    <?php if ( $error !== false ): ?>
        <div class="error">
            <pre><?php echo print_r( $error, true ) ; ?>
            </pre>
        </div>
    <?php endif ; ?>
    <main>

        <form action="#" method="post">
            <?php wp_nonce_field( GF_ASYNC_SLUG_ . '_config', GF_ASYNC_SLUG_ . '_nonce' ) ; ?>
            <button class="gfa-form-submit" type="submit" value="font-selector" name="gfa-form-config" >
                <?php echo __( 'Mettre à jour la configuration', GF_ASYNC_SLUG ) ; ?>
            </button>
            <h2>
                <?php
                echo __( 'Configuration du plugin', GF_ASYNC_SLUG ) ;
                ?>
            </h2>
            <div class="gfa-block-config">
                <h3>Google Fonts</h3>
                <p class="gfa-block-hint"><?php echo __( 'Description du service + URL', GF_ASYNC_SLUG ) ; ?></p>

                <div class="gfa-inline-field">
                    <label for="gfa-google-api"><?php echo __( "API KEY", GF_ASYNC_SLUG ) ; ?></label>
                    <input id="gfa-google-api" name="gfa-google-api" type="text" value="<?php echo $google_apikey ; ?>"/>
                    <p class="gfa-field-hint"><?php echo __( "Veuillez saisir votre clé d'API afin d'accéder au fonts de Google", GF_ASYNC_SLUG ) ; ?></p>
                </div>
                <div class="gfa-inline-field">
                    <label for="gfa-google-keep-result"><?php echo __( "Durée de mise en cache des résultats", GF_ASYNC_SLUG ) ; ?></label>
                    <input id="gfa-google-keep-result" name="gfa-google-keep-result" type="text" value="<?php echo $google_keep_result ; ?>"/>
                    <select id="gfa-google-keep-result-unit" name="gfa-google-keep-result-unit">
                        <option value="s" selected><?php echo __( "secondes", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="m"><?php echo __( "minutes", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="h"><?php echo __( "heures", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="d"><?php echo __( "jours", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="w"><?php echo __( "semaines", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="M"><?php echo __( "mois", GF_ASYNC_SLUG ) ; ?></option>
                    </select>
                    <p class="gfa-field-hint"><?php echo __( "Défini la durée de sauvegarde des résultats.(Permet de limiter le nombre de requête globale)", GF_ASYNC_SLUG ) ; ?></p>
                </div>
            </div>
            <div class="gfa-block-config alpha">
                <h3>TypeKit</h3>
                <p class="gfa-block-hint"><?php echo __( 'Description du service + URL', GF_ASYNC_SLUG ) ; ?></p>

                <div class="gfa-inline-field">
                    <label for="gfa-typekit-api"><?php echo __( "API KEY", GF_ASYNC_SLUG ) ; ?></label>
                    <input id="gfa-typekit-api" name="gfa-typekit-api" type="text" value="<?php echo $typekit_apikey ; ?>"/>
                    <p class="gfa-field-hint"><?php echo __( "Veuillez saisir votre clé d'API", GF_ASYNC_SLUG ) ; ?></p>
                </div>
                <div class="gfa-inline-field">
                    <label for="gfa-typekit-keep-result"><?php echo __( "Durée de mise en cache des résultats", GF_ASYNC_SLUG ) ; ?></label>
                    <input id="gfa-typekit-keep-result" name="gfa-typekit-keep-result" type="text" value="<?php echo $typekit_keep_result ; ?>"/>
                    <select id="gfa-typekit-keep-result-unit" name="gfa-typekit-keep-result-unit">
                        <option value="s" selected><?php echo __( "secondes", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="m"><?php echo __( "minutes", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="h"><?php echo __( "heures", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="d"><?php echo __( "jours", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="w"><?php echo __( "semaines", GF_ASYNC_SLUG ) ; ?></option>
                        <option value="M"><?php echo __( "mois", GF_ASYNC_SLUG ) ; ?></option>
                    </select>
                    <p class="gfa-field-hint"><?php echo __( "Défini la durée de sauvegarde des résultats.(Permet de limiter le nombre de requête globale)", GF_ASYNC_SLUG ) ; ?></p>
                </div>
            </div>
        </form>

        <ul>

        </ul>
    </main>
</div>
