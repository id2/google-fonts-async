<?php
/**
 * Form kit selector
 *
 * Only Typekit
 */
?>
<div class="gfa-wrapper">
    <header>
        <h1>Google Fonts Async</h1>
        <blockquote>La font c'est mon dada! <span>Arnold S.</span></blockquote>
    </header>
    <?php if ( $error !== false ): ?>
        <div class="error">
            <pre><?php echo print_r( $error, true ) ; ?>
            </pre>
        </div>
    <?php endif ; ?>
    <main>

        <form action="#" method="post">
            <?php wp_nonce_field( GF_ASYNC_SLUG_ . '_' . $service, GF_ASYNC_SLUG_ . '_nonce' ) ; ?>
            <button class="gfa-form-submit" type="submit" value="kit-selector" name="gfa-form-font" >
                <?php echo __( 'Mettre à jour le kit à charger', GF_ASYNC_SLUG ) ; ?>
            </button>
            <h2>a
                <?php
                echo __( 'Kit chargeable', GF_ASYNC_SLUG ) ;
                ?>
            </h2>
            <?php echo $kit_selected ; ?>
            <div class="gfa-kit-selector-wrapper">
                <ul>
                    <li>
                        <label><input name="kit" type="radio" <?php echo ($kit_selected == '') ? 'checked' : '' ; ?> value="">Aucun kit</label>
                    </li>
                    <?php foreach ( $kits as $kit ): ?>
                        <li class="kit-selector-item">
                            <label>
                                <input name="kit" type="radio" <?php echo ($kit_selected == $kit->id) ? 'checked' : '' ; ?> value="<?php echo $kit->id ; ?>">
                                <?php echo __( 'Kit n°', GF_ASYNC_SLUG ) ; ?>
                                <em><?php echo $kit->id ; ?></em>
                            </label>
                        </li>
                    <?php endforeach ; ?>
                </ul>
            </div>

        </form>

        <ul>

        </ul>
    </main>
</div>
