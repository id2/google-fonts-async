<?php
/**
 * Form font selector
 *
 * Only for Google Font other like Typekit call fonts kits or collections
 */
$font_category = array() ;
?>
<div class="gfa-wrapper">
    <header>
        <h1>Google Fonts Async</h1>
        <blockquote>La font c'est mon dada! <span>Arnold S.</span></blockquote>
    </header>
    <?php if ( $error !== false ): ?>
        <div class="error">
            <pre><?php echo print_r( $error, true ) ; ?>
            </pre>
        </div>
    <?php endif ; ?>
    <main>

        <form action="#" method="post">
            <?php wp_nonce_field( GF_ASYNC_SLUG_ . '_' . $service, GF_ASYNC_SLUG_ . '_nonce' ) ; ?>
            <button class="gfa-form-submit" type="submit" value="font-selector" name="gfa-form-font" >
                <?php echo __( 'Mettre à jour les fonts à charger', GF_ASYNC_SLUG ) ; ?>
            </button>
            <h2>
                <?php
                echo __( 'Font à charger', GF_ASYNC_SLUG ) ;
                ?>
            </h2>

            <h3 class="gfa-font-filter-title"><?php
                echo __( 'Afficher', GF_ASYNC_SLUG ) ;
                ?></h3>
            <ul class="gfa-font-filter">
                <li class="gfa-font-filter-item is-check">
                    <label><input class="filter filter-reversed" type="checkbox"  data-filter="gfa-just-selected"/><?php echo __( 'Uniquement les sélectionnés', GF_ASYNC_SLUG ) ; ?></label>
                </li>
                <?php
                foreach ( $fonts as $font ) :
                    if ( ! in_array( $font->category, $font_category ) ) {
                        array_push( $font_category, $font->category ) ;
                        ?>
                        <li class="gfa-font-filter-item is-check">
                            <label><input class="filter <?php echo $font->category ?>" type="checkbox" checked data-filter="gfa-not-<?php echo $font->category ?>"/><?php echo $font->category ?></label>
                        </li>
                        <?php
                    }
                endforeach ;
                ?>
            </ul>
            <div class="gfa-font-selector-wrapper">
                <?php
                // include the font selector
                include (GF_ASYNC_VIEWS_PATH . 'form-font-input.php' ) ;
                ?>
            </div>

        </form>

        <ul>

        </ul>
    </main>
</div>
