<?php
/**
 * View generator for field of font selector
 *
 * @TODO rename as specialize google-font-input
 * @TODO add a searchform Javascript for live search
 * @TODO other view filter 
 */
// i is incrementor
$i = 1 ;
?>
<!--<pre><?php var_dump( $fonts_selected ) ; ?></pre>-->

<ul class="gfa-font-list">
    <?php foreach ( $fonts as $font ) :
        ?>
        <li class="gfa-font-item <?php echo $font->category ; ?>">
            <label class="gfa-font-label" for="font<?php echo $i ?>">
                <span class="gfa-font-name"><?php echo $font->family ; ?></span>
                <span class="gfa-font-cat"><?php echo $font->category ; ?></span>
                <span class="gfa-font-choice"></span>

            </label>
            <input class="gfa-font-check" type="checkbox" <?php
            echo 'id="font' . $i . '" name="fonts[' . $font->family . ']" value="' . $font->family . '" ' ;
            if ( isset( $fonts_selected[$font->family] ) ) {
                echo 'checked ' ;
            }
            ?>/>
            <div class="gfa-font-info">
                <div>
                    <h4><?php echo __( 'Variantes', GF_ASYNC_SLUG ) ; ?></h4>
                    <ul class="gfa-font-weight">
                        <?php foreach ( $font->variants as $variant ): ?><li>
                                <label>
                                    <input  type="checkbox" <?php
                                    echo ' name="fonts[' . $font->family . '][weight][]" value="' . $variant . '" ' ;
                                    if ( isset( $fonts_selected[$font->family]['weight'] ) && in_array( $variant, $fonts_selected[$font->family]['weight'] ) ) {
                                        echo 'checked ' ;
                                    }
                                    ?>/>
                                            <?php echo __( $variant, GF_ASYNC_SLUG ) ; ?>
                                </label>
                            </li><?php
                        endforeach ; // variants
                        ?>
                    </ul>
                </div>
                <div>
                    <h4><?php echo __( 'Subsets', GF_ASYNC_SLUG ) ; ?></h4>
                    <ul class="gfa-font-subset">
                        <?php foreach ( $font->subsets as $subset ): ?><li>
                                <label>
                                    <input  type="checkbox" <?php
                                    echo ' name="fonts[' . $font->family . '][subset][]" value="' . $subset . '" ' ;
                                    if ( isset( $fonts_selected[$font->family]['subset'] ) && in_array( $subset, $fonts_selected[$font->family]['subset'] ) ) {
                                        echo 'checked ' ;
                                    }
                                    ?>/>
                                            <?php echo __( $subset, GF_ASYNC_SLUG ) ; ?>
                                </label>
                            </li><?php
                        endforeach ; // subset
                        ?>
                    </ul>
                </div>
            </div>

        </li>
        <?php
        $i ++ ;
    endforeach ;
    ?>
</ul>