<?php

/**
 * Autoload  specific for plugin used by classe
 * @param string $class The fully-qualified class name.
 *
 * without namespace that sucks
 *
 * @return void
 */
spl_autoload_register( function ($class) {

    // Define directory scan order
    $dirs = array(
        '/classes/abstract/', // Abstract classes
        '/classes/interface/', // Interface classes
        '/classes/', // Basics classes
    ) ;

    // Looping through each directory listed to load all the class files. It will only require a file once.
    // When class found break the loop. Wub Wub Wub Wub
    foreach ( $dirs as $dir ) {
        if ( file_exists( __DIR__ . $dir . str_replace( '_', '-', strtolower( $class ) ) . '.php' ) ) {
            require_once( __DIR__ . $dir . str_replace( '_', '-', strtolower( $class ) ) . '.php') ;
            break ;
        }
    }
} ) ;
