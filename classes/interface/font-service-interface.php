<?php

/**
 * Interface Font_Service_Interface
 */
// after PHP 5.3 and rename PSR2
// namespace
// namespace GoogleFontAsync\Classes\Interface ;

/**
 * Make an interface for Font service like Google Font or TypeKit
 *
 *
 * @author id2
 */
interface Font_Service_Interface
{

    /**
     * Prepare a JSON part for the Webloader JS
     *
     * @param array $param
     */
    static public function get_Json( $param ) ;
}
