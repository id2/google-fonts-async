<?php

/**
 * Class Google_Font
 *
 */
// after PHP 5.3 and rename PSR2
// namespace
// namespace GoogleFontAsync\Classes ;
// use GoogleFontAsync\Classes\Abstract\FontService ;
// use GoogleFontAsync\Classes\Interface\FontServiceInterface ;

/**
 * Class for discuss with Google Font API
 *
 * @author id2
 */
class Google_Font extends Font_Service implements Font_Service_Interface
{

    const VERSION_CLASS = 0.1 ;

    /**
     *  API endpoint
     */
    const API_ENDPOINT = 'https://www.googleapis.com/{{interface}}/v{{version}}/{{method}}?key={{apikey}}' ;

    /**
     *  Default version for this API
     */
    const DEFAULT_VERSION = 1 ;

    /**
     *  string API key
     */
    private $apiKey = '' ;

    /**
     * Class constructor
     *
     * @param string API key
     * @return void
     */
    private function __construct( $apiKey )
    {
        $this->apiKey = $apiKey ;
    }

    /**
     * Unique class' instance
     *
     * @param string API key
     * @return GoogleFont
     */
    public static function getInstance( $apiKey = null )
    {
        if ( is_null( self::$_instance ) && ! is_null( $apiKey ) ) {
            self::$_instance = new Google_Font( $apiKey ) ;
        }

        return self::$_instance ;
    }

    /**
     * Prepare URL for the API endpoint
     *
     * @param string $interface
     * @param string $method
     * @param string $apiKey
     * @param string $format
     * @param mixed $version
     * @param array $param
     * @return string URL
     */
    private function generateEndpoint(
    $interface, $method, $apiKey, $format = DEFAULT_FORMAT, $version = DEFAULT_VERSION, $param = null )
    {
        $urlPlaceholder = array(
            "{{interface}}",
            "{{method}}",
            "{{version}}",
            "{{apikey}}",
            "{{format}}"
        ) ;
        $urlReplace = array(
            $interface,
            $method,
            $version,
            $apiKey,
            $format
        ) ;

        $url = str_replace( $urlPlaceholder, $urlReplace, self::API_ENDPOINT ) ;
        if ( ! is_null( $param ) && is_array( $param ) ) {
            $i = 0 ;
            foreach ( $param as $p => $v ) {
                if ( $i == 0 ) {
                    $url.= "?$p=$v" ;
                }
                else {
                    $url.= "&$p=$v" ;
                }
                $i ++ ;
            }
        }
        return $url ;
    }

    /**
     * Request All font from Google
     *
     * @param  bool $popularity get font by popularity
     * @return type
     */
    public function get_all_font( $popularity = false )
    {
        $interface = 'webfonts' ;
        $method = 'webfonts' ;
        $version = 1 ;
        if ( $popularity ) {
            $param = array(
                'sort' => 'popularity',
            ) ;
        }


        $url = $this->generateEndpoint( $interface, $method, $this->apiKey, 'json', $version, $param ) ;

        return $this->http_message( $url ) ;
    }

    /**
     * Transform an array in JSON part for Webloader
     *
     * @param array $param a font list to format
     * @return string JSON to add in webloader
     */
    static public function get_Json( $param )
    {

        $js = 'google: { families: [' ;
        $first = true ;
        foreach ( $param as $font => $data ) {

            if ( true != $first ) {
                $js .= ',' ;
            }
            else {
                $first = false ;
            }
            // check if index exist and if it's all
            $subset = (isset( $data['subset'] )) ? ':' . implode( ',', $data['subset'] ) : '' ;
            $subset = ($subset === ':all') ? '' : $subset ;
            $weight = (isset( $data['weight'] )) ? ':' . implode( ',', $data['weight'] ) : '' ;
            $weight = ($weight === ':all') ? '' : $weight ;

            // add Family to load
            $js .= '"' . $font . $weight . $subset . '"' ;
        }
        $js .= ']}' ;
        return $js ;
    }

}
