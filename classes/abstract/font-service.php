<?php

/**
 * Abstract Font_Service
 * Abstract class for all font_service like Google Fonts or Typekit
 */
// after PHP 5.3 and rename PSR2
// namespace
// namespace GoogleFontAsync\Classes\Abstract ;
//use GoogleFontAsync\Classes\Interface\FontServiceInterface;

/**
 * Mother class for Font services
 * 
 *
 * @author id2
 */
abstract class Font_Service implements Font_Service_Interface
{

    /**
     *  API endpoint
     */
    const API_ENDPOINT = null ;

    /**
     * @var mixed a single instance of the class
     */
    protected static $_instance = null ;

    /**
     * No friend ? Take a cURL(y) to get them :)
     *
     * @param string $url The URL
     * @param string $method send via GET or POST
     * @param string $post_data POST data if needed
     * @return mixed depends on URL call
     * */
    function http_message( $url, $method = 'GET', $post_data = null )
    {
        try {
            $hm = curl_init() ;

            curl_setopt( $hm, CURLOPT_URL, self::API_ENDPOINT . $url ) ;
            curl_setopt( $hm, CURLOPT_RETURNTRANSFER, true ) ;
            curl_setopt( $hm, CURLOPT_TIMEOUT, 60 ) ;
            curl_setopt( $hm, CURLOPT_FOLLOWLOCATION, true ) ;

            // only to send information
            if ( 'POST' == $method ) {
                curl_setopt( $hm, CURLOPT_POST, true ) ;
                curl_setopt( $hm, CURLOPT_POSTFIELDS, $post_data ) ;
            }

            $response = json_decode( curl_exec( $hm ) ) ;
            $error = curl_error( $hm ) ;
            $http_status = curl_getinfo( $hm, CURLINFO_HTTP_CODE ) ;

            curl_close( $hm ) ;
        } catch ( Exception $e ) {
            return array(
                "code" => -1,
                "message" => 'cURL FAIL badly'
            ) ;
        }

        if ( 200 != $http_status && isset( $response->code, $response->detail ) ) {
            return array(
                "code" => $response->code,
                "message" => $response->detail
            ) ; // error with response
        }
        elseif ( 200 != $http_status ) {
            return array(
                "code" => $http_status
            ) ; // error
        }
        else {
            return $response ;
        }

        return $response ;
    }

}
