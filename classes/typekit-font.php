<?php

/**
 * Class Typekit_Font
 *
 */
// after PHP 5.3 and rename PSR2
// namespace
// namespace GoogleFontAsync\Classes ;
// use GoogleFontAsync\Classes\Abstract\FontService ;
// use GoogleFontAsync\Classes\Interface\FontServiceInterface ;

/**
 * Class for discuss with Typekit API
 *
 * @author id2
 */
class Typekit_Font extends Font_Service implements Font_Service_Interface
{

    const VERSION_CLASS = 0.1 ;

    /**
     *  API endpoint
     */
    const API_ENDPOINT = 'https://typekit.com/api/v{{version}}/{{format}}/{{method}}' ;
    const DEFAULT_VERSION = 1 ;
    const DEFAULT_FORMAT = 'json' ;

    /**
     *  Propreties
     */
    private $apiKey = '' ;

    /**
     * Constructeur de la classe
     *
     * @param apiKey
     * @return void
     */
    private function __construct( $apiKey )
    {
        $this->apiKey = $apiKey ;
    }

    /**
     * Unique class' instance
     *
     * @param void
     * @return TypekitFont
     */
    public static function getInstance( $apiKey = null )
    {
        if ( is_null( self::$_instance ) && ! is_null( $apiKey ) ) {
            self::$_instance = new Typekit_Font( $apiKey ) ;
        }

        return self::$_instance ;
    }

    /**
     * Prepare URL for the API endpoint
     *
     * @param string $method
     * @param string $apiKey
     * @param string $format
     * @param mixed $version
     * @param array $param
     * @return string URL
     */
    private function generateEndpoint(
    $method, $apiKey, $format = DEFAULT_FORMAT, $version = DEFAULT_VERSION, $param = null )
    {
        $urlPlaceholder = array(
            "{{method}}",
            "{{version}}",
            "{{apikey}}",
            "{{format}}"
        ) ;
        $urlReplace = array(
            $method,
            $version,
            $apiKey,
            $format
        ) ;

        $url = str_replace( $urlPlaceholder, $urlReplace, self::API_ENDPOINT ) ;
        if ( ! is_null( $param ) && is_array( $param ) ) {
            $i = 0 ;
            foreach ( $param as $p => $v ) {
                if ( $i == 0 ) {
                    $url.= "?$p=$v" ;
                }
                else {
                    $url.= "&$p=$v" ;
                }
                $i ++ ;
            }
        }
        return $url ;
    }

    /**
     *  Return the cURL result of endpoint for get kits from Typekit
     *
     * @param  array|bool $param add param
     * @return type
     */
    public function get_all_kits( $param = false )
    {

        $method = 'kits' ;
        $version = 1 ;
        if ( false === $param ) {
            $param = array(
                'token' => $this->apiKey
            ) ;
        }
        $url = $this->generateEndpoint( $method, $this->apiKey, 'json', $version, $param ) ;

        return $this->http_message( $url ) ;
    }

    /**
     * Return the line for JSON WebFontLoader
     *
     * @param string $kit_id
     * @return mixed bool for error else string
     */
    static public function get_Json( $kit_id = false )
    {
        if ( ! $kit_id ) {
            return false ;
        }
        return 'typekit: { id: "' . $kit_id . '" }' ;
    }

}
