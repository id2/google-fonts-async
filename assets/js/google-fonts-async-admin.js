/**
 * Google Fonts Async 
 * javascript for font selection
 * 
 */
jQuery(function($){
  
  //generic check it parent (target)
  function checkIt( $elem, $target) {
    if ( $elem.is(':checked') ){
        $target.addClass('is-check');
    } else {
        $target.removeClass('is-check');
    }
  }
  
  var $fontWrapper = $('.gfa-font-selector-wrapper');
  
  //init state
  $('.gfa-font-filter-item [type="checkbox"]').each(function(){
    var $self = $(this),
        $target = $(this).closest('.gfa-font-filter-item');
    checkIt($self, $target);
     if ( ( !$self.is(':checked') && !$self.hasClass('filter-reversed') ) || ( $self.is(':checked') && $self.hasClass('filter-reversed') ) ){
        $fontWrapper.addClass($self.data('filter') );  
    } else {
        $fontWrapper.removeClass($self.data('filter') );  
    }
  
  });
  $('.gfa-font-check').each(function(){
    var $self = $(this),
        $target = $(this).closest('.gfa-font-item');
    checkIt($self, $target);
  });
  // Events 
  $('.gfa-font-filter-item [type="checkbox"]').on('change', function(e){
    e.preventDefault();
    var $self = $(this),
    $target = $(this).closest('.gfa-font-filter-item');
    checkIt($self, $target);
     if ( ( !$self.is(':checked') && !$self.hasClass('filter-reversed') ) || ( $self.is(':checked') && $self.hasClass('filter-reversed') ) ){
        $fontWrapper.addClass($self.data('filter') );  
    } else {
        $fontWrapper.removeClass($self.data('filter') );  
    }
  });
  $('.gfa-font-check').on('change', function(e){
    e.preventDefault();
    var $self = $(this),
    $target = $(this).closest('.gfa-font-item');
    checkIt($self, $target);
  });

});