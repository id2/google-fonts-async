<?php

/**
 * Functions / Controller on the front side of the force
 */
defined( 'ABSPATH' ) or die( 'Doh?' ) ;

/**
 * Register and enqueue script for the front
 */
function google_fonts_async_front_load()
{
    // WebFontLoader
    wp_register_script( GF_ASYNC_SLUG . '-webfontload', 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js', false, '1.5.18' ) ;

    // config generate for WebFontLoader by fonts selector in admin
    wp_register_script( GF_ASYNC_SLUG . '-webfontload-config', GF_ASYNC_ASSETS_URL . 'js/' . GF_ASYNC_SLUG . '-webloader.js', GF_ASYNC_SLUG . '-webfontload', 1 ) ;

    // @TODO add in parameter if fontobserver is needed
    if ( false ) {
        // load font observer fontfaceobserver.standalone.js
        wp_register_script( 'fontfaceobserver', GF_ASYNC_ASSETS_URL . 'js/fontfaceobserver.standalone.js', false, '1.6.1' ) ;
        // config generate for font observer
        wp_register_script( GF_ASYNC_SLUG . '-webfont-observer', GF_ASYNC_ASSETS_URL . 'js/' . GF_ASYNC_SLUG . '-font-observer.js', 'fontfaceobserver', 1 ) ;
    }

    // enqueue script in the front ( not separate )
    wp_enqueue_script( GF_ASYNC_SLUG . '-webfontload' ) ;
    wp_enqueue_script( GF_ASYNC_SLUG . '-webfontload-config' ) ;
}

add_action( 'wp_enqueue_scripts', GF_ASYNC_SLUG_ . '_front_load' ) ;
