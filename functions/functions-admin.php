<?php

/**
 * Functions / Controller on the admin side of the force
 */
defined( 'ABSPATH' ) or die( 'Doh?' ) ;

require_once( GF_ASYNC_PATH . '_autoload52.php') ;

/**
 * Register and enqueue script for backOffice
 */
function google_fonts_async_back_load()
{
    //get CSS
    wp_register_style( GF_ASYNC_SLUG . '-admin-style', GF_ASYNC_ASSETS_URL . 'css/' . GF_ASYNC_SLUG . '-admin.css' ) ;
    //get JS
    wp_register_script( GF_ASYNC_SLUG . '-admin-js', GF_ASYNC_ASSETS_URL . 'js/' . GF_ASYNC_SLUG . '-admin.js' ) ;
}

add_action( 'admin_init', GF_ASYNC_SLUG_ . '_back_load' ) ;

/**
 * Get common admin script and style
 */
function google_fonts_async_back_common()
{

    //get CSS
    wp_enqueue_style( GF_ASYNC_SLUG . '-admin-style' ) ;
    //get JS
    wp_enqueue_script( 'jquery' ) ;
    wp_enqueue_script( GF_ASYNC_SLUG . '-admin-js' ) ;
}

/**
 * Add Google Font Settings in sidebar
 */
function google_fonts_async_settings_menu()
{
    // get settings
    $settings = get_option( GF_ASYNC_SETTINGS ) ;

    //page de paramètre
    add_menu_page( 'Google Fonts Async', 'Google Fonts Async', 'manage_options', GF_ASYNC_SLUG ) ;
    add_submenu_page( GF_ASYNC_SLUG, 'Paramètres', 'Paramètres', 'manage_options', GF_ASYNC_SLUG, GF_ASYNC_SLUG_ . '_get_config'
    ) ;

    if ( $settings['google']['apikey'] !== '' ) {
        add_submenu_page( GF_ASYNC_SLUG, 'Google Fonts Selector', 'Google Fonts', 'manage_options', GF_ASYNC_SLUG . '-gf-selector', GF_ASYNC_SLUG_ . '_get_googlefonts' ) ;
    }
    if ( $settings['typekit']['apikey'] !== '' ) {
        add_submenu_page( GF_ASYNC_SLUG, 'TypeKit Fonts Selector', 'TypeKit', 'manage_options', GF_ASYNC_SLUG . '-tk-selector', GF_ASYNC_SLUG_ . '_get_typekit' ) ;
    }
}

add_action( 'admin_menu', GF_ASYNC_SLUG_ . '_settings_menu' ) ;

/**
 * Function call for plugin's config page
 */
function google_fonts_async_get_config()
{
    google_fonts_async_back_common() ;
    include GF_ASYNC_ADMIN_PATH . "config.php" ;
    google_fonts_async_options_page() ;
}

/**
 * Function call for plugin's Google Fonts page
 */
function google_fonts_async_get_googlefonts()
{
    google_fonts_async_back_common() ;
    include GF_ASYNC_ADMIN_PATH . "google-fonts.php" ;
    google_fonts_async_googlefonts_page() ;
}

/**
 * Function call for plugin's Typekit page
 */
function google_fonts_async_get_typekit()
{
    google_fonts_async_back_common() ;
    include GF_ASYNC_ADMIN_PATH . "typekit.php" ;
    google_fonts_async_typekit_page() ;
}

/**
 *  regenerate the JS file
 *
 * @param array $fonts_list array or collecion of font
 */
function google_fonts_async_update_file( $fonts_list = null )
{
    // @TODO update for provider
    if ( is_null( $fonts_list ) ) {
        return new WP_Error( 'fail', __( "Doh' pas de liste de fonts reçu ?", GF_ASYNC_SLUG_ ) ) ;
    }
    // need for get Typekit API key
    $plugin_param = get_option( GF_ASYNC_SETTINGS ) ;

    $js = 'WebFont.load({' ;

    if ( ! empty( $fonts_list['google'] ) ) {
        $js .= Google_Font::get_Json( $fonts_list['google'] ) ;
    }
    if ( ! empty( $fonts_list['typekit'] ) ) {
        if ( ! empty( $fonts_list['google'] ) ) {
            $js .= ', ' ;
        }
        $js .= TypeKit_Font::get_Json( $fonts_list['typekit'] ) ;
    }
    $js .= '});' ;

    file_put_contents( GF_ASYNC_ASSETS_PATH . 'js/' . GF_ASYNC_SLUG . '-webloader.js', $js ) ;

    return true ;
}
