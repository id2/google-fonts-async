<?php

/**
 * Functions specialize for plugin config page
 */

/**
 *  Generate HTML and process form for the page config
 *  @return string HTML of page config
 */
function google_fonts_async_options_page()
{
    $error = false ; //receive error
    $fonts_selected = array() ; //receive selected fonts list
    // form is submit
    if ( isset( $_POST[GF_ASYNC_SLUG_ . '_nonce'] ) ) {

        if ( wp_verify_nonce( filter_input( INPUT_POST, GF_ASYNC_SLUG_ . '_nonce' ), GF_ASYNC_SLUG_ . '_config' ) ) {
            $fonts_selected = call_user_func( GF_ASYNC_SLUG_ . '_update_config' ) ;
            //if param where update
        }
        else {
            $error = new WP_Error( 'fail', __( "Erreur : le formulaire n'a pas été pris en compte. Veuillez réessayer.", GF_ASYNC_SLUG_ ) ) ;
        }
    }
    $plugin_param = get_option( GF_ASYNC_SETTINGS ) ;
    //var_dump( $plugin_param ) ;
    $google_apikey = (isset( $plugin_param['google']['apikey'] )) ? $plugin_param['google']['apikey'] : '' ;
    $google_keep_result = (isset( $plugin_param['google']['keep_result'] )) ? $plugin_param['google']['keep_result'] : DAY_IN_SECONDS ;

    //typekit
    $typekit_apikey = (isset( $plugin_param['typekit']['apikey'] )) ? $plugin_param['typekit']['apikey'] : '' ;
    $typekit_keep_result = (isset( $plugin_param['typekit']['keep_result'] )) ? $plugin_param['typekit']['keep_result'] : DAY_IN_SECONDS ;


    ob_start() ;
    // get view
    include ( GF_ASYNC_VIEWS_PATH . 'plugin-param.php' ) ;
    $html = ob_get_clean() ;

    // process
    echo $html ;
}

/**
 *  Param submission for config update
 */
function google_fonts_async_update_config()
{

    // Google
    $google_apikey = filter_input( INPUT_POST, 'gfa-google-api' ) ;
    $google_keep_result = filter_input( INPUT_POST, 'gfa-google-keep-result' ) ;
    $google_keep_result_unit = filter_input( INPUT_POST, 'gfa-google-keep-result-unit' ) ;

// TypeKit
    $typekit_apikey = filter_input( INPUT_POST, 'gfa-typekit-api' ) ;

    $typekit_keep_result = filter_input( INPUT_POST, 'gfa-typekit-keep-result' ) ;
    $typekit_keep_result_unit = filter_input( INPUT_POST, 'gfa-typekit-keep-result-unit' ) ;

    $multiplicate_unit = array(
        's' => 1,
        'm' => 60,
        'h' => 60 * 60,
        'd' => 60 * 60 * 24,
        'w' => 60 * 60 * 24 * 7,
        'M' => 60 * 60 * 24 * 30, // 30 days not a real month
    ) ;

    // get settings
    $settings = get_option( GF_ASYNC_SETTINGS ) ;

    // modify Google config
    $settings['google']['apikey'] = $google_apikey ;
    $settings['google']['keep_result'] = $google_keep_result * $multiplicate_unit[$google_keep_result_unit] ;
    //refresh google transient
    $transient_google = get_transient( GF_ASYNC_SLUG_ . '_googlefonts_font_result' ) ;
    set_transient( GF_ASYNC_SLUG_ . '_googlefonts_font_result', $transient_google, $settings['typekit']['keep_result'] ) ;

    // modify TypeKit config
    $settings['typekit']['apikey'] = $typekit_apikey ;
    $settings['typekit']['keep_result'] = $typekit_keep_result * $multiplicate_unit[$typekit_keep_result_unit] ;
    $transient_typekit = get_transient( GF_ASYNC_SLUG_ . '_typekit_font_result' ) ;
    set_transient( GF_ASYNC_SLUG_ . '_typekit_font_result', $transient_typekit, $settings['typekit']['keep_result'] ) ;
    // update in database
    update_option( GF_ASYNC_SETTINGS, $settings ) ;
}
