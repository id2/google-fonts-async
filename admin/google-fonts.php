<?php

/**
 * Functions specialize for Google Fonts page
 */

/**
 *  Generate HTML and process form for the page Google
 *  @return string HTML of page Google Fonts
 */
function google_fonts_async_googlefonts_page()
{
    $error = false ; //receive error
    $service = 'googlefonts' ;
    // form is submit
    if ( isset( $_POST[GF_ASYNC_SLUG_ . '_nonce'] ) ) {

        if ( wp_verify_nonce( filter_input( INPUT_POST, GF_ASYNC_SLUG_ . '_nonce' ), GF_ASYNC_SLUG_ . '_' . $service ) ) {

            $fonts_selected = call_user_func( GF_ASYNC_SLUG_ . '_update_google' ) ;
        }
        else {

            $error = new WP_Error( 'fail', __( "Erreur : le formulaire n'a pas été pris en compte. Veuillez réessayer.", GF_ASYNC_SLUG_ ) ) ;
        }
    }

    $plugin_param = get_option( GF_ASYNC_SETTINGS ) ;
    $fonts_selected = ( ! empty( $plugin_param['fonts-selected']['google'] ) ) ? $plugin_param['fonts-selected']['google'] : array() ; //receive selected fonts list


    if ( ! get_transient( GF_ASYNC_SLUG_ . '_' . $service . '_font_result' ) ) {
        //get all font
        $font_service = Google_Font::getInstance( $plugin_param['google']['apikey'] ) ;
        $fonts = $font_service->get_all_font( true )->items ;

        // save result for (default) 1 day
        set_transient( GF_ASYNC_SLUG_ . '_' . $service . '_font_result', $fonts, $plugin_param['google']['keep_result'] ) ;
    }
    else {
        // get the last result for Google Font
        $fonts = get_transient( GF_ASYNC_SLUG_ . '_' . $service . '_font_result' ) ;
    }

    ob_start() ;
    // get view
    include ( GF_ASYNC_VIEWS_PATH . 'font-selector.php' ) ;
    $html = ob_get_clean() ;

    // process
    echo $html ;
}

/**
 *  Param submission for Google font list update
 */
function google_fonts_async_update_google()
{
    $fonts_list = array() ;
    if ( ! isset( $_POST['fonts'] ) || ! is_array( $_POST['fonts'] ) || empty( $_POST['fonts'] ) ) {
        $fonts_list = array() ;
    }
    else {
        $fonts_list = $_POST['fonts'] ;
    }

    // get settings
    $settings = get_option( GF_ASYNC_SETTINGS ) ;

    // modify fonts list for provider
    $settings['fonts-selected']['google'] = $fonts_list ;

    // update in database
    update_option( GF_ASYNC_SETTINGS, $settings ) ;

    // regenerate the js file
    google_fonts_async_update_file( $settings['fonts-selected'] ) ;

    return $fonts_list ;
}
