<?php

/**
 * Functions specialize for Typekit Fonts page
 *
 *
 * @TODO make the kit selector shiny ans flashy
 * @version alpha
 */

/**
 *  Generate HTML and process form for the page Typekit
 *  @return string HTML of page TypeKit
 */
function google_fonts_async_typekit_page()
{
    $error = false ; //receive error
    $service = 'typekit' ;
    // form is submit
    if ( isset( $_POST[GF_ASYNC_SLUG_ . '_nonce'] ) ) {

        if ( wp_verify_nonce( filter_input( INPUT_POST, GF_ASYNC_SLUG_ . '_nonce' ), GF_ASYNC_SLUG_ . '_' . $service ) ) {
            $kit_selected = call_user_func( GF_ASYNC_SLUG_ . '_update_typekit' ) ;
        }
        else {
            $error = new WP_Error( 'fail', __( "Erreur : le formulaire n'a pas été pris en compte. Veuillez réessayer.", GF_ASYNC_SLUG_ ) ) ;
        }
    }

    $plugin_param = get_option( GF_ASYNC_SETTINGS ) ;
    $kit_selected = (isset( $plugin_param['fonts-selected']['typekit'] )) ? $plugin_param['fonts-selected']['typekit'] : array() ; //receive selected fonts list

    if ( ! get_transient( GF_ASYNC_SLUG_ . '_' . $service . '_font_result' ) ) {
        //get all font
        $font_service = Typekit_Font::getInstance( $plugin_param['typekit']['apikey'] ) ;
        $kits = $font_service->get_all_kits()->kits ;
        // save result for 1 day by default
        set_transient( GF_ASYNC_SLUG_ . '_' . $service . '_font_result', $kits, $plugin_param['typekit']['keep_result'] ) ;
    }
    else {
        // get the last result for Google Font
        $kits = get_transient( GF_ASYNC_SLUG_ . '_' . $service . '_font_result' ) ;
    }

    ob_start() ;
    // get view
    include ( GF_ASYNC_VIEWS_PATH . 'kit-selector.php' ) ;
    $html = ob_get_clean() ;

    // process
    echo $html ;
}

/**
 *  Param submission for typekit kit update
 */
function google_fonts_async_update_typekit()
{
    $kit = '' ;
    if ( ! isset( $_POST['kit'] ) || is_null( $_POST['kit'] ) ) {
        $kit = '' ;
    }
    else {
        $kit = $_POST['kit'] ;
    }
    // get settings
    $settings = get_option( GF_ASYNC_SETTINGS ) ;

    // modify the font list
    $settings['fonts-selected']['typekit'] = $kit ;

    // update in database
    update_option( GF_ASYNC_SETTINGS, $settings ) ;

    // regenerate the js file
    google_fonts_async_update_file( $settings['fonts-selected'] ) ;

    return $kit ;
}
